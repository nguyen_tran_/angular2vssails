System.register(['angular2/core', 'angular2/http'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
        switch (arguments.length) {
            case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
            case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
            case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
        }
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1;
    var AccountStore;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            }],
        execute: function() {
            AccountStore = (function () {
                function AccountStore(http) {
                    this.http = http;
                }
                AccountStore.prototype.getAccounts = function () {
                    return this.http.get('/account/getAccounts').map(function (res) { return res.json(); });
                };
                AccountStore.prototype.removeAccount = function (_account) {
                    var headers = new http_1.Headers();
                    headers.append('Content-Type', 'application/json');
                    return this.http.post('/account/removeAccount', JSON.stringify({ id: _account.id }), { headers: headers }).map(function (res) { return res.json(); });
                };
                AccountStore.prototype.saveAccount = function (account) {
                    var headers = new http_1.Headers();
                    headers.append('Content-Type', 'application/json');
                    return this.http.post('/account/addAccount', JSON.stringify({ "account": account }), { headers: headers }).map(function (res) { return res.json(); });
                };
                AccountStore = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], AccountStore);
                return AccountStore;
            })();
            exports_1("AccountStore", AccountStore);
        }
    }
});
//# sourceMappingURL=store.js.map