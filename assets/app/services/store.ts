import {Injectable} from 'angular2/core';
import {Http, Response, Headers} from 'angular2/http';
import {Observable} from 'rxjs/Rx';


@Injectable()
export class AccountStore {
  constructor(private http: Http) {

  }
  getAccounts() {

    return this.http.get('/account/getAccounts').map((res:Response) => res.json());
  }

  removeAccount(_account) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('/account/removeAccount', JSON.stringify({id: _account.id}), {headers:headers}).map((res:Response) => res.json());
  }

  saveAccount(account) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('/account/addAccount', JSON.stringify({"account": account}), {headers:headers}).map((res:Response) => res.json());
  }
}


