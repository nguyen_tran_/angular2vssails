import {bootstrap} from 'angular2/platform/browser';
import {HTTP_PROVIDERS} from 'angular2/http';
import 'rxjs/add/operator/map';
import AccountApp from './app';
import {AccountStore} from './services/store';

bootstrap(AccountApp, [HTTP_PROVIDERS, AccountStore]);
