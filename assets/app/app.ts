import {Component, Input, ChangeDetectionStrategy, EventEmitter} from 'angular2/core';
import {AccountStore} from './services/store';
import {SearchPipe} from './search-pipe';
import {SearchBox} from "./search-box";
import {Observable} from 'rxjs/Observable';

@Component({
	selector: 'account-app',
    pipes: [SearchPipe],
    directives: [SearchBox],
	templateUrl: 'app/app.html',
    events: ['accountChange'],
    changeDetection: ChangeDetectionStrategy.OnPushObserve,
})
export default class AccountApp {
  @Input() term;
  accountChange = new EventEmitter();
  public accounts;
  public tmpAccount = {name: '', telephone: '', email: ''}
  public account = {name: '', telephone: '', email: ''}
	constructor(private _accountStore: AccountStore) {this.getAccounts();}

  ngOnInit() {
    var validateForm = $('#form-account').validate();
    $('#myModal').on('show.bs.modal', function() {
      validateForm.resetForm();
    })

  }

  getAccounts() {
    this._accountStore.getAccounts().subscribe(
      // the first argument is a function which runs on success
      data => { this.accounts = data},
      // the second argument is a function which runs on error
      err => console.error(err),
      // the third argument is a function which runs on completion
      () => console.log('done loading accounts')
    );
  }

  edit(acc) {
    this.account = acc;
    this.tmpAccount.name = acc.name;
    this.tmpAccount.telephone = acc.telephone;
    this.tmpAccount.email = acc.email;
  }

  remove(acc, index) {
    this.accounts.splice(index, 1);
    this._accountStore.removeAccount(acc).subscribe(
      // the first argument is a function which runs on success
      data => { },
      // the second argument is a function which runs on error
      err => {alert('Delet error');window.location.reload(true);console.error(err)},
      // the third argument is a function which runs on completion
      () => {  }
    );
  }
  cancel() {
    this.account.name = this.tmpAccount.name;
    this.account.telephone = this.tmpAccount.telephone;
    this.account.email = this.tmpAccount.email;
  }

  save() {
    if ($('#form-account').valid()) {
      $('#myModal').modal('hide');
      if(this.account.id == undefined) {
        this.accounts.push(this.account)
      }
      this._accountStore.saveAccount(this.account).subscribe(
        // the first argument is a function which runs on success
        data => { if(this.account.id == undefined) this.accounts[this.accounts.length - 1]['id'] = data.id},
        // the second argument is a function which runs on error
        err => console.error(err),
        // the third argument is a function which runs on completion
        () => console.log('done loading accounts')
      );
    }

  }

  createAccount() {
    this.account = {name: '', telephone: '', email: ''}
  }




}
