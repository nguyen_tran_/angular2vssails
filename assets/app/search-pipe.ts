import {Pipe, PipeTransform} from 'angular2/core';

@Pipe({
    name: 'search',
    pure: false
})

export class SearchPipe implements PipeTransform{
    tmp = [];
    private fetchPromise:Promise<any>;
    transform(value, [term]) {
        this.tmp.length = 0;
        var arr = []
        if (value == undefined || term == undefined) {
            if (!this.fetchPromise) {
                this.fetchPromise = window.fetch('/account/getAccounts')
                    .then((result:any) => result.json())
                    .then((json:any)   => arr = json);
            }

        }
        if(value != undefined && term != undefined) {

            arr = value.filter((item) => item.name.includes(term.toLowerCase()) ||item.telephone.includes(term.toLowerCase()) || item.email.includes(term.toLowerCase()) )
        }
        for (var i =0; i < arr.length; ++i) {
            this.tmp.push(arr[i]);
        }
        console.log('tmp', this.tmp)
        return this.tmp;
    }
}