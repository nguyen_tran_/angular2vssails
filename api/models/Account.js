module.exports = {
  attributes: {
    name: {
      'type': 'string',
      required: true
    },
    telephone: {
      type: 'string',
      required: true

    },
    email: {
      type: 'string',
      unique: true,
      required: true
    }
  }
};
