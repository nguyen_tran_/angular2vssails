System.register(['angular2/core'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
        switch (arguments.length) {
            case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
            case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
            case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
        }
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var SearchPipe;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            SearchPipe = (function () {
                function SearchPipe() {
                    this.tmp = [];
                }
                SearchPipe.prototype.transform = function (value, _a) {
                    var term = _a[0];
                    this.tmp.length = 0;
                    var arr = [];
                    if (value == undefined || term == undefined) {
                        if (!this.fetchPromise) {
                            this.fetchPromise = window.fetch('/account/getAccounts')
                                .then(function (result) { return result.json(); })
                                .then(function (json) { return arr = json; });
                        }
                    }
                    if (value != undefined && term != undefined) {
                        arr = value.filter(function (item) { return item.name.includes(term.toLowerCase()) || item.telephone.includes(term.toLowerCase()) || item.email.includes(term.toLowerCase()); });
                    }
                    for (var i = 0; i < arr.length; ++i) {
                        this.tmp.push(arr[i]);
                    }
                    console.log('tmp', this.tmp);
                    return this.tmp;
                };
                SearchPipe = __decorate([
                    core_1.Pipe({
                        name: 'search',
                        pure: false
                    }), 
                    __metadata('design:paramtypes', [])
                ], SearchPipe);
                return SearchPipe;
            })();
            exports_1("SearchPipe", SearchPipe);
        }
    }
});
//# sourceMappingURL=search-pipe.js.map