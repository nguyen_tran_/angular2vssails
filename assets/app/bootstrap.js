System.register(['angular2/platform/browser', 'angular2/http', 'rxjs/add/operator/map', './app', './services/store'], function(exports_1) {
    var browser_1, http_1, app_1, store_1;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {},
            function (app_1_1) {
                app_1 = app_1_1;
            },
            function (store_1_1) {
                store_1 = store_1_1;
            }],
        execute: function() {
            browser_1.bootstrap(app_1.default, [http_1.HTTP_PROVIDERS, store_1.AccountStore]);
        }
    }
});
//# sourceMappingURL=bootstrap.js.map