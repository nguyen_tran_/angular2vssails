import {Component, Output, EventEmitter} from 'angular2/core';

@Component({
    'selector': 'search-box',
    'template': '<div class="col-md-2 pull-right" style="padding: 0"> <input #input (input)="update.emit(input.value)" type="text" class="form-control" placeholder="Search"> </div>'
})

export class SearchBox{
    @Output() update = new EventEmitter();

    ngOnInit() {

        this.update.emit('');

    }
}