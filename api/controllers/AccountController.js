module.exports = {
    getAccounts: function(req, res) {
        AccountService.getAccount(function(accounts) {
            res.json(accounts)
        })
    },
    addAccount: function(req, res) {
        console.log(req.body.account);
        var account = (req.body.account) ? req.body.account : undefined

        if (account.id != undefined && account.id != '') {
          AccountService.updateAccount(account, function(success) {
            res.json(success)
          })
        } else {
          AccountService.addAccount(account, function(success) {
            res.json(success)
          })
        }
    },
    removeAccount: function(req, res) {
       var id = (req.body.id) ? req.body.id : undefined
        AccountService.removeAccount(id, function(success) {
            res.json(success)
        })
    }
}
