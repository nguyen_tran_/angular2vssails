System.register(['angular2/core', './services/store', './search-pipe', "./search-box"], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
        switch (arguments.length) {
            case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
            case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
            case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
        }
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, store_1, search_pipe_1, search_box_1;
    var AccountApp;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (store_1_1) {
                store_1 = store_1_1;
            },
            function (search_pipe_1_1) {
                search_pipe_1 = search_pipe_1_1;
            },
            function (search_box_1_1) {
                search_box_1 = search_box_1_1;
            }],
        execute: function() {
            AccountApp = (function () {
                function AccountApp(_accountStore) {
                    this._accountStore = _accountStore;
                    this.accountChange = new core_1.EventEmitter();
                    this.tmpAccount = { name: '', telephone: '', email: '' };
                    this.account = { name: '', telephone: '', email: '' };
                    this.getAccounts();
                }
                AccountApp.prototype.ngOnInit = function () {
                    var validateForm = $('#form-account').validate();
                    $('#myModal').on('show.bs.modal', function () {
                        validateForm.resetForm();
                    });
                };
                AccountApp.prototype.getAccounts = function () {
                    var _this = this;
                    this._accountStore.getAccounts().subscribe(
                    // the first argument is a function which runs on success
                    function (data) { _this.accounts = data; }, 
                    // the second argument is a function which runs on error
                    function (err) { return console.error(err); }, 
                    // the third argument is a function which runs on completion
                    function () { return console.log('done loading accounts'); });
                };
                AccountApp.prototype.edit = function (acc) {
                    this.account = acc;
                    this.tmpAccount.name = acc.name;
                    this.tmpAccount.telephone = acc.telephone;
                    this.tmpAccount.email = acc.email;
                };
                AccountApp.prototype.remove = function (acc, index) {
                    this.accounts.splice(index, 1);
                    this._accountStore.removeAccount(acc).subscribe(
                    // the first argument is a function which runs on success
                    function (data) { }, 
                    // the second argument is a function which runs on error
                    function (err) { alert('Delet error'); window.location.reload(true); console.error(err); }, 
                    // the third argument is a function which runs on completion
                    function () { });
                };
                AccountApp.prototype.cancel = function () {
                    this.account.name = this.tmpAccount.name;
                    this.account.telephone = this.tmpAccount.telephone;
                    this.account.email = this.tmpAccount.email;
                };
                AccountApp.prototype.save = function () {
                    var _this = this;
                    if ($('#form-account').valid()) {
                        $('#myModal').modal('hide');
                        if (this.account.id == undefined) {
                            this.accounts.push(this.account);
                        }
                        this._accountStore.saveAccount(this.account).subscribe(
                        // the first argument is a function which runs on success
                        function (data) { if (_this.account.id == undefined)
                            _this.accounts[_this.accounts.length - 1]['id'] = data.id; }, 
                        // the second argument is a function which runs on error
                        function (err) { return console.error(err); }, 
                        // the third argument is a function which runs on completion
                        function () { return console.log('done loading accounts'); });
                    }
                };
                AccountApp.prototype.createAccount = function () {
                    this.account = { name: '', telephone: '', email: '' };
                };
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Object)
                ], AccountApp.prototype, "term");
                AccountApp = __decorate([
                    core_1.Component({
                        selector: 'account-app',
                        pipes: [search_pipe_1.SearchPipe],
                        directives: [search_box_1.SearchBox],
                        templateUrl: 'app/app.html',
                        events: ['accountChange'],
                        changeDetection: core_1.ChangeDetectionStrategy.OnPushObserve,
                    }), 
                    __metadata('design:paramtypes', [store_1.AccountStore])
                ], AccountApp);
                return AccountApp;
            })();
            exports_1("default", AccountApp);
        }
    }
});
//# sourceMappingURL=app.js.map