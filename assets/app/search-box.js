System.register(['angular2/core'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
        switch (arguments.length) {
            case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
            case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
            case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
        }
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var SearchBox;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            SearchBox = (function () {
                function SearchBox() {
                    this.update = new core_1.EventEmitter();
                }
                SearchBox.prototype.ngOnInit = function () {
                    this.update.emit('');
                };
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', Object)
                ], SearchBox.prototype, "update");
                SearchBox = __decorate([
                    core_1.Component({
                        'selector': 'search-box',
                        'template': '<div class="col-md-2 pull-right" style="padding: 0"> <input #input (input)="update.emit(input.value)" type="text" class="form-control" placeholder="Search"> </div>'
                    }), 
                    __metadata('design:paramtypes', [])
                ], SearchBox);
                return SearchBox;
            })();
            exports_1("SearchBox", SearchBox);
        }
    }
});
//# sourceMappingURL=search-box.js.map