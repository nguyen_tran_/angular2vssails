# Demo Application in Angular2.js + Sails.js + PostgresSQL

#Setup Application

1. Install PostgreSQL 9.5.1: http://www.postgresql.org/
2. Create Database as config ./config/connections.js and then create table account: name, telephone, email.
3. Run command: npm install
4. Run application by command: sails lift

Access application: http://localhost:1337
