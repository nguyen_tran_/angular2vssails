module.exports = {
  addAccount: function(account, next) {
    console.log('account', account)
    Account.create({
      name: account.name,
      telephone: account.telephone,
      email: account.email
    }).exec(function(err, acc) {
      console.log('error', err)
      if(err) throw err;
      next(acc);

    })
  },
  removeAccount: function(id, next) {
    Account.destroy({id: id}).exec(function(err, acc) {
      if(err) throw err;
      next(acc);
    })
  },
  updateAccount: function(account, next) {
      Account.update({id: account.id}, {name: account.name, telephone: account.telephone, email: account.email }).exec(function(err, acc) {
        if(err) throw err;
        next(acc);
      })
  },
  getAccount: function(next) {
    Account.find().exec(function(err, accounts) {
      if(err) throw err;
      next(accounts);
    })
  }
}
